from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.core.mail import send_mail


from .forms import BandForm, ContactsUsForm, ListingForm

from .models import Band, Listing


def page_not_found(request, exception):
    print(exception)
    return render(request, "404.html")


def email_sent(request):
    print(f"client ip = {get_client_ip(request)}")
    return render(request, "listings/email_sent.html")


def band_list(request):
    bands = Band.objects.all()
    print(f"client ip = {get_client_ip(request)}")
    return render(request, "listings/band_list.html", {"bands": bands})


def band_detail(request, band_id):
    band = get_object_or_404(Band, pk=band_id)
    return render(request, "listings/band_detail.html", {"band": band})


def band_update(request: HttpRequest, band_id: int):
    band = get_object_or_404(Band, pk=band_id)
    if request.method == "POST":
        form = BandForm(request.POST, instance=band)
        if form.is_valid():
            form.save()
            return redirect("band_detail", band.id)
    else:
        form = BandForm(instance=band)
    return render(request, "listings/band_update.html", {"form": form})


def band_create(request: HttpRequest):
    if request.method == "POST":
        form = BandForm(request.POST)
        if form.is_valid():
            band = form.save()
            return redirect("band_detail", band.id)
    else:
        form = BandForm()
    return render(request, "listings/band_create.html", {"form": form})


def band_delete(request, band_id):
    band = Band.objects.get(id=band_id)
    if request.method == "POST":
        band.delete()
        return redirect("band_list")
    return render(request, "listings/band_delete.html", {"band": band})


def listings(request):
    listings = Listing.objects.all()
    if len(listings) == 0:
        titles = [
            "Affiche ORIGINALE de la tournée de De La Soul - Fillmore Auditorium San Francisco novembre 2001",
            "T-shirt du concert de Cut Copy, tournée Free Your Mind, 2013",
            "Foo Fighters - l’affiche promo du single Big Me, fin des années 90",
            "Beethoven - Sonate au clair de lune - manuscrit original EXTRÊMEMENT RARE",
        ]
        for title in titles:
            Listing.objects.create(title=title)
        listings = Listing.objects.all()
    print(f"client ip = {get_client_ip(request)}")
    return render(request, "listings/listings.html", {"listings": listings})


def listing_detail(request, listing_id):
    listing = get_object_or_404(Listing, pk=listing_id)
    return render(request, "listings/listing_detail.html", {"listing": listing})


def listing_update(request: HttpRequest, listing_id: int):
    listing = get_object_or_404(Listing, pk=listing_id)
    if request.method == "POST":
        form = ListingForm(request.POST, instance=listing)
        if form.is_valid():
            form.save()
            return redirect("listing_detail", listing.id)
    else:
        form = ListingForm(instance=listing)
    return render(request, "listings/listing_update.html", {"form": form})


def listing_create(request: HttpRequest):
    if request.method == "POST":
        form = ListingForm(request.POST)
        if form.is_valid():
            listing = form.save()
            return redirect("listing_detail", listing.id)
    else:
        form = ListingForm()
    return render(request, "listings/listing_create.html", {"form": form})


def listing_delete(request, listing_id: str):
    listing = Listing.objects.get(id=listing_id)
    if request.method == "POST":
        listing.delete()
        return redirect("listing_list")
    return render(request, "listings/listing_delete.html", {"listing": listing})


def contact(request):
    print(f"client ip = {get_client_ip(request)}")
    print("La méthode de requête est : ", request.method)
    print("Les données POST sont : ", request.POST)
    if request.method == "POST":
        form = ContactsUsForm(request.POST)
        if form.is_valid():
            send_mail(
                subject=f"message from {form.cleaned_data['name'] or 'anonyme'} via Merchex Contact Us form",
                message=form.cleaned_data["message"],
                from_email=form.cleaned_data["email"],
                recipient_list=["admin@merchex.xyz"],
            )
            return redirect("email_sent")
    else:
        form = ContactsUsForm()
    return render(request, "listings/contact.html", {"form": form})


def about(request):
    print(f"client ip = {get_client_ip(request)}")
    return render(request, "about.html")


def get_client_ip(request):
    x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
    if x_forwarded_for:
        ip = x_forwarded_for.split(",")[0]
    else:
        ip = request.META.get("REMOTE_ADDR")
    return ip
