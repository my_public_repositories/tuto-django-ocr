from django.contrib import admin
from django.apps import apps

# from django.contrib.auth import Permissions
from .models import User

Permission = apps.get_model("auth", "Permission")

admin.site.register(User)
admin.site.register(Permission)
