# Generated by Django 4.2.3 on 2023-08-06 11:24

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("authentication", "0003_auto_20230805_2325"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="follows",
            field=models.ManyToManyField(
                limit_choices_to={"role": "CREATOR"}, to=settings.AUTH_USER_MODEL, verbose_name="suit"
            ),
        ),
    ]
