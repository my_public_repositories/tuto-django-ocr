from time import strftime
from django import template
from django.utils import timezone
from datetime import datetime, timedelta

register = template.Library()


@register.filter
def model_type(value):
    return type(value).__name__


@register.simple_tag(takes_context=True)
def get_poster_display(context, user):
    if user == context["user"]:
        return "vous"
    return user.username


@register.filter
def get_posted_at_display(value: datetime):
    now = timezone.now()
    elapsed_time: timedelta = now - value
    if elapsed_time.days > 0:
        return f"Posté à {value.strftime('%H:%M %d %m %Y')}"
    elapsed_hours = divmod(elapsed_time.seconds, 3600)[0]
    if elapsed_hours > 0:
        return f"Posté il y a {elapsed_hours} heures"
    elpased_minutes = divmod(elapsed_time.seconds, 60)[0]
    return f"Posté il y a {elpased_minutes} minutes"
