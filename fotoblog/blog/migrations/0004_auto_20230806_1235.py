# Generated by Django 4.2.3 on 2023-08-06 12:35

from django.db import migrations


def migrate_author_to_contributors(apps, schema_editor):
    Blog = apps.get_model("blog", "Blog")
    for blog in Blog.objects.all():
        if blog.author:
            blog.contributors.add(blog.author, through_defaults={"contribution": "Auteur principal"})


class Migration(migrations.Migration):
    dependencies = [
        ("blog", "0003_alter_blog_author_blogcontributor_blog_contributors"),
    ]

    operations = [migrations.RunPython(migrate_author_to_contributors)]
