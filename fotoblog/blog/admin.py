from django.contrib import admin
from .models import Blog, BlogContributor

admin.site.register(Blog)
admin.site.register(BlogContributor)
